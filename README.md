# README

This is a backup of the "hypercomplex" online group, prior to Yahoo taking it offline on 15 Dec 2020.

Group link (as of 25 Feb 2020, now defunct): https://groups.yahoo.com/neo/groups/hypercomplex/info


## Continued

This group is now continued at https://groups.io/g/hypercomplex


## In this repo

* _YahooHyperxomplex_Posts_1500_Dec2019_till_end.pdf_ - Overview and detail messages compiled by John Shuster, from the "hypercomplex" Yahoo group message number 1500 through the end (dating roughly December 2019 through December 2020) 
* _Files/_ - A sundry list of files that members uploaded to one of the discussion groups. John Shuster and I downloaded them manually, so they're probably not complete, but include a detailed message history compiled by John.
* _YahooBackupHypercomplex/_ - Yahoo's backup extract of the "hypercomplex" group (August 2006 through November 2019), initially moderated by me, then later by John Shuster. The group was created to continue Kevin Carmody's "hypernumber" group.
* _YahooBackupHypernumber/_ - The "hypernumber" group moderated by Armahedi Mahzar, also to continue the original group by Kevin Carmody.
* _YahooBackupHypernumber/files/_ - A message extract from Kevin Carmody's original online group, compiled by Arma, dating back to April 2006 (post number 239)

